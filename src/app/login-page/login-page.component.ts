import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from '../Service/auth-service.service';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {


  public password: any;

  constructor(private authservise: AuthServiceService, private router:Router) { }
  public username:number | undefined;

  public Login(){
      this.authservise.loginAuth(this.username,this.password);
  }
  ngOnInit(): void {
    this.authservise.isLoggedIn.subscribe((isLoggedIn)=>{
      if(isLoggedIn){
        this.router.navigate(["/employee-details"])
      }
    })
  }
loginForm= new FormGroup({
  EmployeeId: new FormControl('',[Validators.required]),
  Password: new FormControl('',[Validators.required])
})
  

}
