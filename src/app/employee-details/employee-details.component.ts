import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MdbModalRef, MdbModalService } from 'mdb-angular-ui-kit/modal';
import { isEmpty } from 'rxjs';
import { AddEmployeeModalComponent } from '../add-employee-modal/add-employee-modal.component';
import { Iemployee } from '../Interfaces/iemployee';
import { GetdetailsService } from '../Service/getdetails.service';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  currentEmployee!: Iemployee;
  decodedToken:any;
  adminOrNot!: boolean;
  allEmployees: Iemployee[] = [];
  constructor(private jwtHelper:JwtHelperService, private Employees:GetdetailsService,private modalService: MdbModalService) { }
  modalRef: MdbModalRef<AddEmployeeModalComponent> | null = null
  ngOnInit(): void {
    let token:any=localStorage.getItem('JwtToken');
    this.decodedToken =this.jwtHelper.decodeToken(token);
    if(parseInt(this.decodedToken.role)==7||parseInt(this.decodedToken.role)==8){
      this.adminOrNot=true;
      console.log(this.adminOrNot)
      this.fetchEmployees();
      
    }
    else{
      this.adminOrNot=false
      console.log(this.adminOrNot)
    }

    this.Employees.getUserById(parseInt(this.decodedToken.unique_name)).subscribe((x)=>{
      this.currentEmployee=x;
      console.log(this.currentEmployee);
     

    })
  }
  openModal() {
    this.modalRef = this.modalService.open(AddEmployeeModalComponent);
    this.modalRef.onClose.subscribe(({refresh}: {refresh: boolean}) =>{
      if(refresh) {
        this.fetchEmployees();
      }
    })
  }

  private fetchEmployees() {
    this.Employees.getAllDetails().subscribe((x)=>{
      this.allEmployees=x;
      console.log(this.allEmployees)
    })
  }


}
