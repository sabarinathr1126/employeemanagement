import { Component, OnInit } from '@angular/core';
import { MdbModalRef } from 'mdb-angular-ui-kit/modal';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GetdetailsService } from '../Service/getdetails.service';
import { Iemployee } from '../Interfaces/iemployee';

@Component({
  selector: 'ng add mdb-angular-ui-kitadd-employee-modal',
  templateUrl: './add-employee-modal.component.html',
  styleUrls: ['./add-employee-modal.component.css']
})
export class AddEmployeeModalComponent implements OnInit {

  _EmployeeName:any;
  _Address:any;
  _Password:any;
  _TeamNumber:any;
  _PhoneNumber:any;
  constructor(public modalRef: MdbModalRef<AddEmployeeModalComponent>,private EmpService:GetdetailsService) {}
  AppForm=new FormGroup(
    {
      EmployeeName:new FormControl('',[Validators.required]),
      TeamNumber:new FormControl('',[Validators.required]),
      PhoneNumber:new FormControl('',[Validators.required]),
      Address:new FormControl('',[Validators.required]),
      Password:new FormControl('',[Validators.required]),
    }
  );

  ngOnInit(): void {
  }
  addEmployee(){
      let employee : Iemployee = {
        employeeName:this.AppForm.value.EmployeeName||'',
        address:this.AppForm.value.Address||'',
        phoneNumder:parseInt(this.AppForm.value.PhoneNumber||''),
        teamNumber:parseInt(this.AppForm.value.TeamNumber||''),
        password:this.AppForm.value.Password||''
        
      }
      this.EmpService.AddNewEmployee(employee)
      .subscribe((x: any)=>{console.log(x) ;this.onClose(true)});
      
    }

    onClose(value: boolean) {
      this.modalRef.close({refresh: value});
    }

}
