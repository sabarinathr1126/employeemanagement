import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import{HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http:HttpClient) { }

  public loginSubject$ = new BehaviorSubject(false);
  public isLoggedIn = this.loginSubject$.asObservable();
  JwtToken:any;
  flag:any;

  loginAuth(username:any,password:any){
    // console.log(this.employeeList)

    this.http.post("https://localhost:7033/api/Login",{username,password},{responseType:'text'}).subscribe((response)=>{
    this.JwtToken=response;
    console.log(response);
    localStorage.setItem('JwtToken',JSON.stringify(this.JwtToken))
    this.loginSubject$.next(true);
   },error=>{this.loginSubject$.next(false);
      console.log(error)}
   )
   
  }

  logout(){
    this.loginSubject$.next(false);
    localStorage.removeItem('token');
  }
}
