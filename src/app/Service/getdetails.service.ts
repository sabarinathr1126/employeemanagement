import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Iemployee } from '../Interfaces/iemployee';

@Injectable({
  providedIn: 'root'
})
export class GetdetailsService {

  constructor(private http:HttpClient) { }

  getUserById(Id : number):Observable<Iemployee>{
  return this.http.get<Iemployee>(`https://localhost:7033/id?id=${Id}`);
  }

  getAllDetails():Observable<Iemployee[]>{
    return this.http.get<Iemployee[]>("https://localhost:7033/users");
  }

  AddNewEmployee(empLoyee:Iemployee ):any{
     return this.http.post("https://localhost:7033/api/Employees/post",empLoyee)

  }
}
