import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../Service/auth-service.service';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(private auth:AuthServiceService) { }

  logout(){
    this.auth.logout();
}

  loginStatus:any;

  ngOnInit(): void {

    this.auth.isLoggedIn.subscribe((isLoggedIn)=>{
      this.loginStatus=isLoggedIn;
    })
  }


}
